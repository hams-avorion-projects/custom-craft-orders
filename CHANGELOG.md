# Changelog
### 1.2.1
* Updated mod for Avorion 1.0

### 1.2.0
* Adding commands via scripts is now possible. If you do so, there is no need to adjust galaxy configs

### 1.1.0
* Removed backwards compatibility to Avorion 0.23 or lower
* Add temporary fix for Windows backward slashes inherited from vanilla `craftorders.lua`

### 1.0.1
* Add compatibility to Avorion 0.24.*

### 1.0.0
* Update mod for Steam Workshop

### 0.2.1
* Support Avorion 0.21.x

### 0.2.0
Initial release