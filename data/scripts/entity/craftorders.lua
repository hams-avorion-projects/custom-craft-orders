
package.path = package.path .. ";data/scripts/lib/?.lua"
package.path = package.path .. ";data/scripts/lib/ConfigLib/?.lua"

include ("stringutility")
include ("faction")
include ("callable")
include ("ordertypes")

include ("utility")

local ConfigLib = include("ConfigLib")
local CCOConfigLib = ConfigLib("1723585481")

local CCOLib = include("CCOLib")

local ordersLoaded = false


local ordersindex = {
	title = 1,
	fnc = 2,
}

local CustomCommands = {}


function CraftOrders.removeSpecialOrders()
    local entity = Entity()

    for index, name in pairs(entity:getScripts()) do
        if string.match(name, "data.scripts.entity.ai.") then
            entity:removeScript(index)
        end
    end
end

CraftOrders.CustomCraftOrdersInitialize = CraftOrders.initialize
function CraftOrders.initialize()
	-- Execute vanilla function
	CraftOrders.CustomCraftOrdersInitialize()
	
	-- Load orders if on server. Client need to fetch config from server first
	if onServer() and type(CCOLib.getModdedOrders()) == "table" and #CCOLib.getModdedOrders() then
		CraftOrders.loadCustomOrders()
	end
end

function CraftOrders.loadCustomOrders()
	local customorders = CCOLib.getModdedOrders()
	if type(customorders) == "table" then
		ordersLoaded = true
		for k,order in ipairs(customorders) do
			if CraftOrders.validFunctionName(order[ordersindex.fnc]) then
				CCOConfigLib.log(3, "Initialize custom order:", order[ordersindex.title], order[ordersindex.fnc])
				-- Dynamicly initialize the function for the button to invoke functions from external mods
				--CustomFunction = CustomCommands[order[ordersindex.fnc]]
				CraftOrders[order[ordersindex.fnc]] = function()
					CCOConfigLib.log(4, "Run custom function:", order[ordersindex.fnc])
					if onClient() then
						invokeServerFunction(order[ordersindex.fnc])
						ScriptUI():stopInteraction()
						return
					end
					
					if checkCaptain() then
						CraftOrders.removeSpecialOrders()
						ShipAI():setIdle()
						
						
						
						if order[ordersindex.fnc] and CustomCommands[order[ordersindex.fnc]] then
							CustomCommands[order[ordersindex.fnc]]()
							CCOConfigLib.log(4, "Executed custom function:", order[ordersindex.fnc])
						else
							ShipAI():setPassive()
							CCOConfigLib.log(1, "Could not execute custom command. Please check your configs")
						end
					end
				end
				callable(CraftOrders, order[ordersindex.fnc])
			else
				CCOConfigLib.log(1, "Invalid function name:", order[ordersindex.fnc])
			end
			
		end
	end
end

-- create all required UI elements for the client side
function CraftOrders.initUI()
	if onClient() and not ordersLoaded then
		CraftOrders.loadCustomOrders()
	end
	
	CCOConfigLib.log(4, "Initialize CCO UI")
	local odersSum = CCOLib.getOrdersSum()
	
    local res = getResolution()
    local size = vec2(250, CCOLib.getUiHeight())

    local menu = ScriptUI()
    local window = menu:createWindow(Rect(res * 0.5 - size * 0.5, res * 0.5 + size * 0.5))
    menu:registerWindow(window, "Orders"%_t)

    window.caption = "Craft Orders"%_t
    window.showCloseButton = 1
    window.moveable = 1
	
	

    local splitter = UIHorizontalMultiSplitter(Rect(window.size), 10, 10, odersSum - 1) --We need n-1 splitters for n rows
	
	local i = 0
	
	
	-- Indices are strings for vanilla commands
	for _,order in ipairs(CCOLib.getActiveVanillaOrders()) do
		window:createButton(splitter:partition(i), order.title%_t, order.fnc)
		i = i + 1
	end
	
	local customorders = CCOLib.getModdedOrders()
	if customorders == 0 then customorders = {} end
	CCOConfigLib.log(4, #customorders, "custom orders detected")
	if #customorders then
		for k,order in ipairs(customorders) do
			if CraftOrders.validFunctionName(order[ordersindex.fnc]) then
				window:createButton(splitter:partition(i), order[ordersindex.title]%_t, order[ordersindex.fnc])
				i = i + 1
			end
		end
	end
end


function CraftOrders.validFunctionName(name)
	if type(name) == "string" then
		return string.match(name, "^CCO")
	end
end

