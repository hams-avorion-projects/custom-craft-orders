local orders = {}

local addOrder = function(name, fnc)
	if type(name) == "string" and type(fnc == "string") then
		table.insert(orders, {name, fnc})
	end
end

return orders