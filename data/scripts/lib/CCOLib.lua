
package.path = package.path .. ";data/scripts/lib/?.lua"
package.path = package.path .. ";data/scripts/lib/ConfigLib/?.lua"

local CCOLib = {}

local ConfigLib = include("ConfigLib")
local CCOConfigLib = ConfigLib("1723585481") -- Mod ID is configured in modinfo.lua

local moddedOrders = nil -- All modded orders
local scriptedOrders = include("CustomCraftOrders") -- Orders added by scripts

-- Get the total amount of different orders
function CCOLib.getOrdersSum()
	local CustomOrders = CCOLib.getModdedOrders()
	if CustomOrders == 0 then CustomOrders = {} end
	
	return #CCOLib.getActiveVanillaOrders() + #CustomOrders
end

-- Get height of order UI Element
function CCOLib.getUiHeight()
	return CCOLib.getOrdersSum() * 40 -- 40 pixel per order button
end

-- Get active Vanilla orders
function CCOLib.getActiveVanillaOrders()
	local aCom = {} -- active orders
	
	local disabledOrders = {}
	if CCOConfigLib.get("disabledOrders") ~= 0 then
		for _,v in pairs(CCOConfigLib.get("disabledOrders")) do
			disabledOrders[v] = true
		end
	end
	
	for _,com in ipairs(CCOLib.getVanillaOrders()) do
		if not disabledOrders[com.title] then
			table.insert(aCom, com)
		end
	end
	
	return aCom
end

-- Get Vanilla orders
function CCOLib.getVanillaOrders()
	return {
		{
			title = "Idle",
			fnc = "onUserIdleOrder",
		},
		{
			title = "Passive",
			fnc = "onUserPassiveOrder",
		},
		{
			title = "Guard This Position",
			fnc = "onUserGuardOrder",
		},
		{
			title = "Patrol Sector",
			fnc = "onUserPatrolOrder",
		},
		{
			title = "Escort Me",
			fnc = "onUserEscortMeOrder",
		},
		{
			title = "Repair Me",
			fnc = "onUserRepairMeOrder",
		},
		{
			title = "Repair",
			fnc = "onUserRepairOrder",
		},
		{
			title = "Attack Enemies",
			fnc = "onUserAttackEnemiesOrder",
		},
		{
			title = "Mine",
			fnc = "onUserMineOrder",
		},
		{
			title = "Salvage",
			fnc = "onUserSalvageOrder",
		},
		{
			title = "Refine Ores",
			fnc = "onUserRefineOresOrder",
		},
	}
end

-- Get Modded orders
function CCOLib.getModdedOrders()
	if moddedOrders == nil then
		local CustomOrders = CCOConfigLib.get("customorders")
		
		if type(CustomOrders) == "table" then
			for _,order in ipairs(CCOConfigLib.get("customorders")) do
				table.insert(scriptedOrders, order)
			end
		end
		
		moddedOrders = scriptedOrders
	end
	
	return moddedOrders
end

return CCOLib