# Custom Craft Orders

This mod is a resource for modders to add custom ship orders to the game. The new ship orders will be available in the order menu when interacting with a ship by pressing `F`.


## Usage
Create a new file in your mod that extends `craftorders.lua`:

    /data/scripts/entity/craftorders.lua

It must contain a function that will be executed when the order button is pressed. Replace `MyCommand` by a valid function name, that represents your order (do *not* remove the `CCO` prefix)

    function CustomCommands.CCOMyCommand ()
        -- Do stuff here

        -- Examples:
        ShipAI():anycommand()
        -- or:
        Entity():addScriptOnce("data/scripts/entity/ai/myCommand.lua")
    end

#### Hints
* When adding a script, this script must be inside of `data/scripts/entity/ai/` or ship commands may break

### Add command to the ship menu
There are two methods to add a new command to the ship menu.

#### 1) By scripts (new, recommanded)
Add a new script to your mod:

    data/scripts/lib/CustomCraftOrders.lua

Content should be like this:

    addOrder("Haulgoods", "CCOMyCommand")

Replace `CCOMyCommand` by the function name, you set in `craftorders.lua`.


##### Pro
* Easy setup - no need to adjust custom any config after you subscribed to the mod


##### Contra

* When using this mod on server, the command mod must **not** be server side only



#### 2) By Galaxy Config
Start and then stop your galaxy, this will create a default config file in your galaxy directory. Open it:

    moddata/ConfigLib/CustomCraftOrders-1723585481.lua

Add informations about your custom script. The example in the config file will help you. The mod needs the following three informations for each order:

* `title` - The ingame title of the mod. Do not translate yet, the mod will do it for you.YourMod` you have to set this to `YourMod`.
* `fnc` - The function to execute the related order. Must start with CCO


##### Pro
* The command mod can be server side only, but will appear in ship command menu

##### Contra
* The mod need to be configured in the galaxy configs


#### Config.DisabledOrders
This mod can also disable one or more vanilla orders. To do so watch the config file inside of your galaxy folder (starting the galaxy once with this mod activated is required):

    moddata/ConfigLib/CustomCraftOrders-1723585481.lua


## Roadmap
* Make orders available on map
* Add order chain support
* Implement sorting of orders in menu

